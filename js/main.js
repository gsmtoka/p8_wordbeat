window.addEventListener('load', init);

//Globals

// Available Levels
const levels = {
    easy: 7,
    medium: 5,
    hard: 3
}

// To Change Level
const currentLevel = levels.easy;

let time = currentLevel;
let score = 0;
let isPlaying;

//DOM elements
const wordInput = document.querySelector('#word-input');
const currentWord = document.querySelector('#current-word');
const scoreDisplay = document.querySelector('#score');
const timeDisplay = document.querySelector('#time');
const message = document.querySelector('#message');
const seconds = document.querySelector('#seconds');

const words = [
    'xositashvili',
    'gorozia',
    'dvalashvili',
    'rexviashvili',
    'tevdoradze',
    'jguburia',
    'lipartia',
    'akofiani',
    'shubitidze',
    'guruli',
    'pochxua',
    'beroshvili',
    'kopaliani',
    'futkaradze',
    'kvinikadze',
    'aladashvili',
    'chanchibadze',
    'gogsadze',
    'megreli',
    'tegerashvili',
    'tabatadze',
    'qutateladze',
    'ghughunishvili',
    'rogava',
    'samakashvili'
]

//Initialize Game
function init() {
    // Show Number Of Seconds In UI
    seconds.innerHTML = currentLevel;
    //Load word from array
    showWord(words);
    // Start matching on word input
    wordInput.addEventListener('input', startMatch);
    // Call countdown every second
    setInterval(countdown, 1000);
    //Check game status
    setInterval(checkStatus, 50);
}

// Start martch
function startMatch() {
    if(matchWords()) {
        isPlaying = true;
        time = currentLevel + 1;
        showWord(words);
        wordInput.value = '';
        score++;
    }

    // If score is -1, display 0
    if (score === -1) {
        scoreDisplay.innerHTML = 0;
    } else {
        scoreDisplay.innerHTML = score;
    }
}

// Match currentWord to wordInput
function matchWords() {
    if(wordInput.value === currentWord.innerHTML) {
        message.innerHTML = 'Correct!!! სწორია!';
        return true;
    } else {
        message.innerHTML = '';
        return false;
    }
}

// Pick & show random word
function showWord(words) {
    // Generate ramdon array index
    const randIndex = Math.floor(Math.random() * words.length);
    //Output random word
    currentWord.innerHTML = words[randIndex];
}

// Countdown timer
function countdown() {
    //Make sure time is not run out
    if(time>0) {
        // Decrement
        time--;
    } else if (time === 0) {
        //Game is over
        isPlaying = false;
    }
    //Show time
    timeDisplay.innerHTML = time;
}

// Check game status
function checkStatus() {
    if(!isPlaying && time === 0) {
        message.innerHTML = 'Game Over!!! თქვენ წააგეთ თამაში!';
        score = -1;
    }
}